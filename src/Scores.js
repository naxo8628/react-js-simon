import React, { Component } from 'react';

class Scores extends Component {

  constructor(props){
    super(props);
    this.state = {
      "scores": []
      ,"hide" : true
    }
  }

  add(name,score){
    var scores = this.state.scores;
    scores.push({"score" : score, "name" : name});
    this.setState({"scores": scores});
  }

  showScores(){
    this.setState({"hide" : !this.state.hide});
  }
  render() {
    return (
      <div className="">
        <button type="button" className="btn btn-default" onClick={this.showScores.bind(this)}>Show scores</button>

          <div id="scores" className="panel panel-success" hidden={this.state.hide}>
            <div  className="panel-body" >
              Scores
            </div>
            <div className="panel-footer" >
              <ul className="nav nav-pills nav-stacked">
                {this.state.scores.map(function(score, index){
                    return <li key={ index }>{score.score} - {score.name}</li>;
                  })}              
              </ul>
            </div>
          </div>
      </div>
    );
  }
}

export default Scores;
