import React, { Component } from 'react';
import './App.css';
import MaxScore from './MaxScore';
import Scores from './Scores';
import Pad from './Pad';
import Contador from './Contador';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      "colors" : ["red","yellow","green","blue","black","white","orange"],
      "numeroPad" : 4,
      "isStart": false,
      "weStartTitle": "Dale a START",
    }
    this.childs = [];
    this.cont = null;
    this.maxScore = null;
    this.scores = null;
    this.secuencia = [];
    this.secuenciaEjecutar = [];
    this.secuenciaUser = [];

  }
  onEndParpadear(){
    if(this.secuenciaEjecutar.length > 0){
      var index = this.secuenciaEjecutar.shift();
      this.childs[index].parpadearSequence();
    }else{
      this.setState({"weStartTitle" : "Tu turno:"});
    }
  }

  startGame(){
    this.setState({"isStart" : true , "weStartTitle" : "Recuerda la serie:"});
    var randomPadIndex = Math.floor((Math.random() * this.state.numeroPad));
    this.secuencia.push(randomPadIndex);
    this.secuenciaEjecutar = this.secuencia.slice();
    this.onEndParpadear();
  }

  checkPad(padIndex){
    if(this.state.isStart){
      this.secuenciaUser.push(padIndex);
      if(this.secuencia[this.secuenciaUser.length - 1] === padIndex){
        if(this.secuencia.length === this.secuenciaUser.length){
          this.cont.up();
          this.secuenciaUser = [];
          this.startGame();
        }
      }else{
        this.restart();
      }
    }
  }
  cleanGame(){
    this.secuencia = [];
    this.secuenciaUser = [];
    this.cont.restart();
    this.setState({"isStart" : false , "weStartTitle" : "FIN DE PARTIDA"});
  }
  restart(){
    var score = this.cont.get();
    this.maxScore.setMaxScore(score);
    var name = window.prompt("[Puntuacion "+score+"] Elige nombre:","");
    this.scores.add(name,score);
    this.cleanGame();
  }

  renderSelect(){
    return (
      <form className="navbar-form navbar-left">
        <div className="form-group">
          <label className="control-label" htmlFor="formInput">Numero de Pads:</label>
          <select id="formInput"
                  className="form-control" 
                  onChange={(event)=>{this.setState({"numeroPad" : event.target.value}); }} 
                  value={this.state.numeroPad}
                  disabled={this.state.isStart}>
            {this.state.colors.map( (color,i) => {
                return (<option key={i} value={i+1}>{i+1}</option>);
              })}
          </select>
      </div>
      </form>
    );
  }

  generateRef(i,instance){
    this.childs.splice(i,0,instance);
    this.childs = this.childs.slice(0,this.state.numeroPad);
  }

  renderButtons() {

    return (
      <div className="btn-group">
        <button className="btn btn-success" onClick={this.startGame.bind(this)} disabled={this.state.isStart}>
          <span className="glyphicon glyphicon-triangle-right"></span> START
        </button> 
        <button className="btn btn-danger" onClick={this.cleanGame.bind(this)} disabled={!this.state.isStart}>
          <span className="glyphicon glyphicon-trash"></span> CLEAN
        </button>
      </div>
        
    );
  }
  

  render() {
    return (
      <div className="App container-fluid">
        <nav className="navbar navbar-default">
          <div className="collapse navbar-collapse">
            <ul className="nav nav-bar">
              <li className="col-sm-3 nav navbar-nav ">
                <a>
                  <MaxScore className="" ref={(ins)=>{this.maxScore = ins}}/>
                </a>
              </li>
              <li className="col-sm-6">
                {this.renderSelect()}
              </li>
              <li className="col-sm-3 nav navbar-nav">
                <a>
                  <Scores ref={(ins)=>{this.scores = ins}} />
                </a>
              </li>
            </ul>
          </div>  
        </nav>
        <Contador ref={(instance) => {this.cont = instance}}></Contador>
        <div id="containerPad" className="row">
          <h2 id="weStart">{this.state.weStartTitle}</h2>
          {this.state.colors.slice(0,this.state.numeroPad).map( (color,i) => {
            return (<Pad ident={i} 
                         onEndParpadear={this.onEndParpadear.bind(this)} 
                         checkPad={this.checkPad.bind(this)} key={i} 
                         ref={this.generateRef.bind(this,i)} color={color} />);
          })}
        </div>

        {this.renderButtons()}
        
      </div>
    );
  }
}

export default App;
