import React, { Component } from 'react';

class MaxScore extends Component {

  constructor(props){
    super(props);
    this.state = {"maxScore" : 0}
  }
  setMaxScore(score){
    if(score > this.state.maxScore){
      this.setState({"maxScore" : score});
    }
  }
  render() {
    return (
      <div className="bg-success">
        Maxima puntuacion: {this.state.maxScore}
      </div>
    );
  }
}

export default MaxScore;
