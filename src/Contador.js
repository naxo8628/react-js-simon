import React, { Component } from 'react';

class Contador extends Component {

  constructor(props){
    super(props);
    this.state = {
      "correctsRounds" : 0
    }

  }
  restart(){
    this.setState({"correctsRounds" : 0 });
  }

  up(){
    this.setState({"correctsRounds" : this.state.correctsRounds + 1});
  }

  get(){
    return this.state.correctsRounds
  }
  render() {
    return (
      <div className="row">
        <h1 className="col-sm-4 bg-primary col-sm-offset-4">
        {this.state.correctsRounds}          
        </h1>
      </div>
    );
  }
}

export default Contador;
