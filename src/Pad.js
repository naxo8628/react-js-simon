import React, { Component } from 'react';

class Pad extends Component {

  constructor(props){
    super(props);
    this.state = {
      "resplandece" : false
    }
  }
  parpadearSequenceCallback(callback){
    this.setState({"resplandece" : true});
    var self = this;
    var c_self = callback;
    setTimeout(function () { 
      self.setState({"resplandece" : false});
      setTimeout(function(){c_self(self.props.ident);},700);
      }, 700);
  }

  parpadear(){
    this.parpadearSequenceCallback(this.props.checkPad);
  }

  parpadearSequence(){
    this.parpadearSequenceCallback(this.props.onEndParpadear);
  }
  render() {

    let isResplandor = ( this.state.resplandece) ? "resplandorblanco" : "";
    return (
        <div className={ "circulo "+this.props.color + " " + isResplandor} height="100" width="100" onClick={this.parpadear.bind(this)} fill={this.props.color} >
        </div>
    );
  }
}

export default Pad;
